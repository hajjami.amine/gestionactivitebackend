package com.gestionactivite.activite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.CrossOrigin;

@SpringBootApplication
@ComponentScan({"com"})
public class ActiviteApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActiviteApplication.class, args);
    }
}

