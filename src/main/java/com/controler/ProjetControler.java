package com.controler;

import com.models.Projet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.services.ProjetService;

import java.util.ArrayList;


@RestController
@CrossOrigin
public class ProjetControler {

    @Autowired
    private ProjetService ps;


    @RequestMapping("/getAllProjets")
    @CrossOrigin
    public ArrayList<Projet> getAll(){
        return ps.getAll();
    }

    @RequestMapping(value="/addProject", method=RequestMethod.POST)
    @CrossOrigin
    public void addProject(@RequestBody Projet projet){
        ps.addProject(projet);
    }


}
