package com.services;

import com.DAO.IProjetDAO;
import com.models.Projet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
@Service
public class ProjetService implements IprojectService {
    @Autowired
    IProjetDAO dao;
    public ArrayList<Projet> getAll(){
        return dao.getAll();
    }
    public void addProject(Projet projet){
        dao.addProject(projet);
    }
}
