package com.controler;

import com.models.User;
import com.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class Authentification {
    @Autowired
    AuthService as;


    @RequestMapping(value="/authValidation", method=RequestMethod.POST)
    @CrossOrigin
    boolean authenValidation(@RequestBody User user){
        return as.authenValidation(user);
    }


}
