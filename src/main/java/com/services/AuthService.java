package com.services;

import com.DAO.IAuthDAO;
import com.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService implements IAuthService{
    @Autowired
    IAuthDAO dao;

    public boolean authenValidation(User user){
        return dao.authenValidation(user);
    };
}

