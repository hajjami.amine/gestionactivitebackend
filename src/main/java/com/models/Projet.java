package com.models;

public class Projet {

    private String reference;
    private String client;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }


    public Projet(String reference, String client) {
        this.reference = reference;
        this.client = client;
    }
}
