package com.models;

public class User {
    private String email;
    private String password;

    public User(String login, String password) {
        this.email = login;
        this.password = password;
    }

    public String getLogin() {
        return email;
    }

    public void setEmail(String login) {
        this.email = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
