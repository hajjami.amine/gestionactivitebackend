package com.services;

import com.models.Projet;

import java.util.ArrayList;


public interface IprojectService {
    ArrayList<Projet> getAll();
    void addProject(Projet projet);
}
