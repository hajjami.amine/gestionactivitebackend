package com.DAO;

import com.models.Projet;

import java.util.ArrayList;

public interface IProjetDAO {
    ArrayList<Projet> getAll();
    void addProject(Projet projet);
}
