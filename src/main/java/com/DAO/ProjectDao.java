package com.DAO;

import com.models.Projet;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
@Repository
public class ProjectDao implements IProjetDAO {
    private ArrayList<Projet> projets;
    public ArrayList<Projet> getAll(){
        projets = new ArrayList<>();
        projets.add(new Projet("test","test"));
        projets.add(new Projet("P1","C1"));
        return projets;
    }
    public void addProject(Projet projet){
        projets.add(projet);
    }

}
