package com.services;

import com.models.User;

public interface IAuthService {
    boolean authenValidation(User user);
}
